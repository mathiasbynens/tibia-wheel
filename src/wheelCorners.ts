/* 
 * This file is part of Tibia Wheel.
 * Copyright (c) 2022 Maciej Sopyło
 * 
 * Tibia Wheel is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * Tibia Wheel is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import tl0 from '../assets/skillwheel/backdrop_skillwheel_largebonus_front0_TL.png?as=webp';
import bl0 from '../assets/skillwheel/backdrop_skillwheel_largebonus_front0_BL.png?as=webp';
import tr0 from '../assets/skillwheel/backdrop_skillwheel_largebonus_front0_TR.png?as=webp';
import br0 from '../assets/skillwheel/backdrop_skillwheel_largebonus_front0_BR.png?as=webp';

import tl1 from '../assets/skillwheel/backdrop_skillwheel_largebonus_front1_TL.png?as=webp';
import bl1 from '../assets/skillwheel/backdrop_skillwheel_largebonus_front1_BL.png?as=webp';
import tr1 from '../assets/skillwheel/backdrop_skillwheel_largebonus_front1_TR.png?as=webp';
import br1 from '../assets/skillwheel/backdrop_skillwheel_largebonus_front1_BR.png?as=webp';

import tl2 from '../assets/skillwheel/backdrop_skillwheel_largebonus_front2_TL.png?as=webp';
import bl2 from '../assets/skillwheel/backdrop_skillwheel_largebonus_front2_BL.png?as=webp';
import tr2 from '../assets/skillwheel/backdrop_skillwheel_largebonus_front2_TR.png?as=webp';
import br2 from '../assets/skillwheel/backdrop_skillwheel_largebonus_front2_BR.png?as=webp';

import tl3 from '../assets/skillwheel/backdrop_skillwheel_largebonus_front3_TL.png?as=webp';
import bl3 from '../assets/skillwheel/backdrop_skillwheel_largebonus_front3_BL.png?as=webp';
import tr3 from '../assets/skillwheel/backdrop_skillwheel_largebonus_front3_TR.png?as=webp';
import br3 from '../assets/skillwheel/backdrop_skillwheel_largebonus_front3_BR.png?as=webp';

export const corners = {
  0: [
    br0,
    bl0,
    tl0,
    tr0,
  ],
  1: [
    br1,
    bl1,
    tl1,
    tr1,
  ],
  2: [
    br2,
    bl2,
    tl2,
    tr2,
  ],
  3: [
    br3,
    bl3,
    tl3,
    tr3,
  ],
};

import b0 from '../assets/skillwheel/backdrop_skillwheel_largebonus_light_BR.png?as=webp'
import b1 from '../assets/skillwheel/backdrop_skillwheel_largebonus_light_BL.png?as=webp'
import b2 from '../assets/skillwheel/backdrop_skillwheel_largebonus_light_TL.png?as=webp'
import b3 from '../assets/skillwheel/backdrop_skillwheel_largebonus_light_TR.png?as=webp'

export const balls = [ b0, b1, b2, b3 ];